// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

using namespace glm;
using namespace std;

#include <common/shader.hpp>
#include <common/objloader.hpp>
#include <common/vboindexer.hpp>

#include <common/gameobject/transform.hpp>

#include <common/gameobject/gameobject.hpp>
#include <common/gameobject/plane.hpp>
#include <common/gameobject/terrainObject.hpp>
#include <common/gameobject/physicObject.hpp>

#include <common/gameobject/terrain.hpp>
#include <common/scene/scene.hpp>

#include <common/controls.hpp>

void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 1024;
const unsigned int SCR_HEIGHT = 768;

// camera
glm::vec3 camera_position   = glm::vec3(0.0f, 0.0f,  3.0f);
glm::vec3 camera_target = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 camera_up    = glm::vec3(0.0f, 1.0f,  0.0f);

GLuint ModelID;
GLuint ViewID;
GLuint ProjID;

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

//rotation
float angle = 0.;
float zoom = 1.;
/*******************************************************************************/

int main( void )
{
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        getchar();
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( 1024, 768, "TP1 - GLFW", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        getchar();
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        return -1;
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // Hide the mouse and enable unlimited mouvement
    //  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, 1024/2, 768/2);

    // Dark blue background
    glClearColor(0.8f, 0.8f, 0.8f, 0.0f);

    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    //glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

    // Cull triangles which normal is not towards the camera
    //glEnable(GL_CULL_FACE);

    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Create and compile our GLSL program from the shaders
    GLuint programID = LoadShaders( "vertex_shader.glsl", "fragment_shader.glsl" );
    // Get a handle for our "LightPosition" uniform
    glUseProgram(programID);
    GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");


    ModelID =  glGetUniformLocation(programID, "modelMatrix");
    ViewID =  glGetUniformLocation(programID, "viewMatrix");
    ProjID =  glGetUniformLocation(programID, "projMatrix");

    /*****************TODO***********************/
    // Get a handle for our "Model View Projection" matrices uniforms

    /****************************************/

    //---------------	TERRAIN Scene 	----------------------

    Scene* terrainScene = new Scene();

    Terrain* terrain = new Terrain(glm::vec3(0,-1,0),10,10,64,64,5); terrain->link(programID,ModelID);
    terrain->setupModel();

    terrain->loadLevelTexture("grass.png",0);
    terrain->loadLevelTexture("rock.png",1);
    terrain->loadLevelTexture("snowrocks.png",2);

    terrain->loadHeightMap("HEIGHT_SEA.pgm");//"Heightmap_Mountain.png");
    terrain->updateGeometry();
    terrainScene->bind(terrain);

    TerrainObject* walkingSuzanne = new TerrainObject("sphere.off",terrain); walkingSuzanne->link(programID,ModelID);
    walkingSuzanne->loadTexture("red.jpg");
    walkingSuzanne->setupModel();
    walkingSuzanne->setTransform(glm::vec3(-4,9,-4),glm::vec3(0,0,0),glm::vec3(0.3,0.3,0.3));
    terrainScene->bind(walkingSuzanne);

    //---------------	PHYSIC Scene 	----------------------

    Scene* physicScene = new Scene();

    Terrain* plane = new Terrain(glm::vec3(0,-1,0),10,10,64,64,5); plane->link(programID,ModelID);
    plane->setupModel();
    plane->loadLevelTexture("grass.png",0);
    plane->updateGeometry();
    physicScene->bind(plane);

    PhysicObject* bouncingBox = new PhysicObject("cube.off",plane); bouncingBox->link(programID,ModelID);
    bouncingBox->loadTexture("red.jpg");
    bouncingBox->setupModel();
    bouncingBox->setTransform(glm::vec3(-4,2,-4),glm::vec3(0,0,0),glm::vec3(1,1,1));
    bouncingBox->setVelocity(glm::vec3(1,0,1));
    bouncingBox->setBounce(0.8f);
    physicScene->bind(bouncingBox);

    //---------------	PLANET Scene 	----------------------

    Scene* planetScene = new Scene();

 	GameObject* sun = new GameObject("suzanne.off"); sun->link(programID,ModelID);
    sun->setupModel();
    sun->setTransform(glm::vec3(0,0,0),glm::vec3(0,0,0),glm::vec3(1,1,1));
    planetScene->bind(sun);

    GameObject* earth = new GameObject("suzanne.off"); earth->link(programID,ModelID);
    earth->setupModel();
    earth->setTransform(sun,glm::vec3(5,0,0),glm::vec3(0,0,0),glm::vec3(0.2,0.2,0.2));
    earth->setParent(sun);

	GameObject* moon = new GameObject("suzanne.off"); moon->link(programID,ModelID);
    moon->setupModel();
    moon->setTransform(earth,glm::vec3(8,0,0),glm::vec3(0,0,0),glm::vec3(0.6,0.6,0.6));
    moon->setParent(earth);


    glUniform1f(glGetUniformLocation(programID, "maxHeight"), terrain->getPeakHeight());

    // For speed computation
    double lastTime = glfwGetTime();
    int nbFrames = 0; int sceneID = 1;

    cout << endl << "STARTING ENGINE" << endl << endl;

    cout << endl << "PRESS '*' to change Scene" << endl << endl;
    cout << endl << "PRESS 'c' to change camera mode" << endl << endl;


    bool changedScene = false;
    do{

        // Measure speed
        // per-frame time logic
        // --------------------
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // input
        // -----
        processInput(window);


        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Use our shader
        glUseProgram(programID);


        /*****************TODO***********************/
        // Model matrix : an identity matrix (model will be at the origin) then change

        // View matrix : camera/view transformation lookat() utiliser camera_position camera_target camera_up

        // Projection matrix : 45 Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units

        // Send our transformation to the currently bound shader,
        // in the "Model View Projection" to the shader uniforms

        /****************************************/

        computeMatricesFromInputs();

        //glm::mat4 modelMatrix = glm::mat4(  1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1);
        glm::mat4 viewMatrix = getViewMatrix();
        glm::mat4 projectionMatrix = getProjectionMatrix();

        //glUniformMatrix4fv(ModelID,1,GL_FALSE,&modelMatrix[0][0]);
        glUniformMatrix4fv(ViewID,1,GL_FALSE,&viewMatrix[0][0]);
        glUniformMatrix4fv(ProjID,1,GL_FALSE,&projectionMatrix[0][0]);
     

        if(sceneID == 0)
        {
        	terrainScene->update(deltaTime);
            walkingSuzanne->move(glm::vec3(0.02,0,0.02));
        }
        else if(sceneID == 1)
        {
            physicScene->update(deltaTime);
        }
        else
        {
        	planetScene->update(deltaTime);
        	sun->rotate(glm::vec3(0,0.01,0));
        	earth->rotate(glm::vec3(0,0.05,0));
        	moon->rotate(glm::vec3(0,0.02,0));
        }


        if (glfwGetKey( window, GLFW_KEY_UP ) == GLFW_PRESS){
            terrain->modifyGeometry(terrain->getWidthCount()+1,terrain->getLenghtCount()+1);
            terrain->updateGeometry();
        }
        if (glfwGetKey( window, GLFW_KEY_DOWN ) == GLFW_PRESS){
            terrain->modifyGeometry(terrain->getWidthCount()-1,terrain->getLenghtCount()-1);
            terrain->updateGeometry();
        }


         if (glfwGetKey( window, GLFW_KEY_KP_MULTIPLY ) == GLFW_PRESS)
         {
         	if(!changedScene)
            {
         		sceneID++;
                if(sceneID >= 3){sceneID=0;}
         	}
         	changedScene = true;
         }else{changedScene=false;}


        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_M ) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0 );

    // Cleanup VBO and shader
    //glDeleteBuffers(1, &vertexbuffer);
    //glDeleteBuffers(1, &elementbuffer);

    glDeleteProgram(programID);
    glDeleteVertexArrays(1, &VertexArrayID);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    return 0;
}


// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    glfwSetInputMode(window,GLFW_CURSOR,GLFW_CURSOR_HIDDEN);

    //Camera zoom in and out
    float cameraSpeed = 2.5 * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera_position += cameraSpeed * camera_target;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera_position -= cameraSpeed * camera_target;


    //TODO add translations

}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}
