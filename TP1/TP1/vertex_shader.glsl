#version 330 core

// Input vertex data, different for all executions of this shader.

layout(location = 0) in vec3 vertices_position_modelspace;
layout(location = 1) in vec2 uvs;

out vec2 uv;
out float height;

uniform mat4 modelMatrix = mat4(1.0, 0.0, 0.0, 0.0,  
                  0.0, 1.0, 0.0, 0.0,  
                  0.0, 0.0, 1.0, 0.0,  
                  0.0, 0.0, 0.0, 1.0);

uniform mat4 viewMatrix = mat4(1.0, 0.0, 0.0, 0.0,  
                  0.0, 1.0, 0.0, 0.0,  
                  0.0, 0.0, 1.0, 0.0,  
                  0.0, 0.0, 0.0, 1.0);

uniform mat4 projMatrix = mat4(1.0, 0.0, 0.0, 0.0,  
                  0.0, 1.0, 0.0, 0.0,  
                  0.0, 0.0, 1.0, 0.0,  
                  0.0, 0.0, 0.0, 1.0);

uniform float maxHeight = 10;

void main(){

        gl_Position = ((projMatrix  * viewMatrix) * modelMatrix) * vec4(vertices_position_modelspace,1);

        uv = uvs;

        height = vertices_position_modelspace.y / maxHeight;

       // gl_Position = vec4(vertices_position_modelspace,1);

}

