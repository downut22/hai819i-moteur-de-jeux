#version 330 core

// Ouput data
out vec4 FragColor;

in vec2 uv;
in float height;

uniform sampler2D mainTexture;
uniform bool useLevelTextures;
uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;

void main()
{
    if(!useLevelTextures)
    {
        FragColor = texture(mainTexture,uv);
    }
    else
    {
        if(height <= 0.3)
        {
            FragColor = texture(texture0,uv);
        }
        else if(height <= 0.9)
        {
            FragColor = texture(texture1,uv);
        }
        else
        {
            FragColor = texture(texture2,uv);
        }
    }

    //FragColor = vec4(height,height,height,height);
}
