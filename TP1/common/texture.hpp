#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include "./stb_image.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Texture
{
private:

	unsigned char* data; int size;
    int width,height,nbChannels;
    bool enabled;

public:
    int getWidth(){return width;}
    int getHeight(){return height;}
    int getChannels(){return nbChannels;}

    unsigned char* getData(){return data;}

    glm::vec3 get(double u, double v)
    {
        return get((int)(u * (width-1)), (int)(v * (height-1)));
    }
    glm::vec3 get(int x, int y)
    {
        if(x < 0 || x >= width){std::cout<<"Width out of bounds ! " << x << std::endl; return glm::vec3(0,0,0);}
        if(y < 0 || y >= height){std::cout<<"Height out of bounds ! " << y << std::endl;return glm::vec3(0,0,0);}

        if(nbChannels == 3)
        {
            return glm::vec3(data[ (x + (y * width))*3] / 255.0,data[(x + (y * width))*3 + 1]/ 255.0,data[(x + (y * width))*3 + 2]/ 255.0);
        }
        else if(nbChannels == 2)
        {

        }
        else
        {
             return glm::vec3(data[ (x + (y * width))] / 255.0,data[ (x + (y * width))] / 255.0,data[ (x + (y * width))] / 255.0);
        }
    }

    Texture(char* path)
    {
        data = stbi_load(path, &width, &height, &nbChannels, 0);
        if (!data) {
            enabled = false;
            std::cout << "Failed to load image at path " << path << std::endl;
        }
        else
        {
            enabled = true; size = width * height;
            std::cout<<"Loaded texture <" << path << "> : width is " << width << " ; height is " << height << " ; nbChannels is " << nbChannels << std::endl;
        }
        //stbi_image_free(dataChar);
    }
    Texture(){enabled = false;}
};

// Load a .BMP file using our custom loader
GLuint loadBMP_custom(const char * imagepath);

//// Since GLFW 3, glfwLoadTexture2D() has been removed. You have to use another texture loading library, 
//// or do it yourself (just like loadBMP_custom and loadDDS)
//// Load a .TGA file using GLFW's own loader
//GLuint loadTGA_glfw(const char * imagepath);

// Load a .DDS file using GLFW's own loader
GLuint loadDDS(const char * imagepath);


#endif
