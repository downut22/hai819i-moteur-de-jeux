#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../texture.hpp"

#include "transform.hpp"

using namespace std;

class GameObject : public Transform
{
protected:
	vector<glm::vec3> vertices;
	vector<glm::vec2> uvs;
	vector<unsigned short> faces_indices;
	vector<std::vector<unsigned short> > faces;

	Texture* texture;
 	GLuint textureId; GLuint textureUniform;

	GLuint vertexbuffer;
	GLuint elementbuffer;
	GLuint uvbuffer;

public:

	virtual void update(float deltatime) override
	{
		draw();
		Transform::update(deltatime);
	}

	virtual void draw();
	virtual void drawTextures();

	void bindTexture(Texture* tex, GLuint& id);
	void loadTexture(char* path){texture = new Texture(path); bindTexture(texture,textureId);}

	void setupModel();

	GameObject() : Transform() {}

	GameObject(char* modelPath);

	GameObject(vector<glm::vec3> vertices,vector<std::vector<unsigned short>> faces,vector<unsigned short> faces_indices);

	virtual ~GameObject();
};


/*
class Plane : public GameObject {
private:

public:

	Plane(int xVertices, int zVertices);
};

*/
#endif
