#ifndef TRANSFORM_H
#define TRANSFORM_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../texture.hpp"

using namespace std;

class Transform
{
protected:

	Transform* m_parent;
	vector<Transform*> childs;

	glm::vec3 m_position;
	glm::vec3 m_scale;
	glm::vec3 m_rotation;

	glm::mat4 m_matrix;

	GLuint programID; GLuint ModelID;
public:

	glm::vec3 position()
	{
		return m_position;
		//if(m_parent == nullptr) return m_position;
		//else return m_position + m_parent->position();
	}

	glm::vec3 scale()
	{
		return m_scale;
		//if(m_parent == nullptr) return m_scale;
		//else return m_scale * m_parent->scale();
	}

	glm::vec3 rotation()
	{
		return m_rotation;
		//if(m_parent == nullptr) return m_rotation;
		//else return m_rotation + m_parent->rotation();
	}

	glm::mat4 matrix()
	{
		m_matrix = glm::mat4(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1);

		m_matrix = glm::translate(m_matrix,position());

		m_matrix = glm::rotate<float>(m_matrix,rotation().x,glm::vec3(1,0,0)); 
		m_matrix = glm::rotate<float>(m_matrix,rotation().y,glm::vec3(0,1,0)); 
		m_matrix = glm::rotate<float>(m_matrix,rotation().z,glm::vec3(0,0,1)); 

		m_matrix = glm::scale(m_matrix,scale());

		if(m_parent == nullptr)
    	{
    		return m_matrix;
    	}
    	else
    	{
    		return m_parent->matrix() * m_matrix;
    	}
	}
	
	void setParent(Transform* p)
	{
		m_parent = p;
		p->bind(this);
	}

	void bind(Transform* t)
	{
		childs.push_back(t);
	}

	void setTransform(Transform* parent,glm::vec3 p, glm::vec3 r, glm::vec3 s)
	{
		this->m_parent = parent;
		this->m_position = p;
		this->m_rotation = r;
		this->m_scale = s;
	}

	void setTransform(glm::vec3 p, glm::vec3 r, glm::vec3 s)
	{
		this->m_position = p;
		this->m_rotation = r;
		this->m_scale = s;
	}

	void setPosition(glm::vec3 p)
	{
		m_position = p;
	}
	void setRotation(glm::vec3 r)
	{
		m_rotation = r;
	}
	void setScale(glm::vec3 s)
	{
		m_scale = s;
	}

	void rotate(glm::vec3 r)
	{
		m_rotation += r;
	}

	void scale(glm::vec3 s)
	{
		m_scale += s;
	}

	void move(glm::vec3 m)
	{
		m_position += m;
	}

	virtual void update(float deltatime)
	{
		for(int i = 0; i < childs.size(); i++)
		{
			childs.at(i)->update(deltatime);
		}
	}

	void link(GLuint programID,GLuint ModelID)
	{
		this->programID = programID;
		this->ModelID = ModelID;
	}

	Transform()
	{
		this->m_parent = nullptr;
		this->m_position = glm::vec3(0,0,0);
		this->m_rotation = glm::vec3(0,0,0);
		this->m_scale = glm::vec3(1,1,1);
	}

	Transform(glm::vec3 p, glm::vec3 r, glm::vec3 s)
	{
		this->m_parent = nullptr;
		this->m_position = p;
		this->m_rotation = r;
		this->m_scale = s;
	}

	Transform(Transform* parent,glm::vec3 p, glm::vec3 r, glm::vec3 s)
	{
		this->m_parent = parent;
		this->m_position = p;
		this->m_rotation = r;
		this->m_scale = s;
	}
};


/*
class Plane : public GameObject {
private:

public:

	Plane(int xVertices, int zVertices);
};

*/
#endif
