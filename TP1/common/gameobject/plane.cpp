#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <common/objloader.hpp>
#include "plane.hpp"

using namespace std;

Plane::Plane(glm::vec3 center, float width, float lenght)
{
	glm::vec3 upLeft = center + glm::vec3(-width/2,lenght/2,0);
	glm::vec3 bottomLeft = center + glm::vec3(-width/2,-lenght/2,0);
	glm::vec3 upRight = center + glm::vec3(width/2,lenght/2,0);
	glm::vec3 bottomRight = center + glm::vec3(width/2,-lenght/2,0);

	vertices.push_back(upLeft);
	vertices.push_back(bottomLeft);
	vertices.push_back(upRight);
	vertices.push_back(bottomRight);

	faces_indices.push_back(0);
	faces_indices.push_back(1);
	faces_indices.push_back(3);

	faces_indices.push_back(0);
	faces_indices.push_back(3);
	faces_indices.push_back(2);
}

Plane::Plane(glm::vec3 upLeft, glm::vec3 bottomLeft,glm::vec3 upRight,glm::vec3 bottomRight)
{
	vertices.push_back(upLeft);
	vertices.push_back(bottomLeft);
	vertices.push_back(upRight);
	vertices.push_back(bottomRight);

	faces_indices.push_back(0);
	faces_indices.push_back(1);
	faces_indices.push_back(3);

	faces_indices.push_back(0);
	faces_indices.push_back(3);
	faces_indices.push_back(2);
}
