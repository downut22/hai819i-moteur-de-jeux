#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <common/objloader.hpp>
#include "terrainObject.hpp"

using namespace std;

void TerrainObject::draw()
{
	GameObject::draw();
}

void TerrainObject::placeOnTerrain()
{
	glm::vec2 uv = terrain->getUV(glm::vec2(m_position.x,m_position.z));

	m_position.y = terrain->getCenter().y + terrain->getHeight(uv) + (m_scale.y); //terrain->position().y +
}

TerrainObject::TerrainObject(char* modelPath,Terrain* _terrain) : GameObject(modelPath)
{
	terrain = _terrain;
}
