#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <common/objloader.hpp>
#include "terrain.hpp"

using namespace std;

void Terrain::draw()
{
	GameObject::draw();
}

void Terrain::drawTextures()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,level0Id);
	glUniform1i(level0Uniform, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D,level1Id);
	glUniform1i(level1Uniform, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D,level2Id);
	glUniform1i(level2Uniform, 2);
}

void Terrain::modifyGeometry(int wCount, int lCount)
{
	widthCount = wCount;
	lenghtCount = lCount;
}

void Terrain::updateGeometry()
{
	vertices.clear();
	faces_indices.clear();
	uvs.clear();

	float wUnit = width / (float)widthCount;
	float lUnit = lenght / (float)lenghtCount;

	for(int x = 0; x < widthCount; x++)
	{
		for(int z = 0; z < lenghtCount; z++)
		{
			float px = (x * wUnit) - (width/2.0);
			float pz = (z * lUnit) - (lenght/2.0);

			glm::vec3 point = glm::vec3(center.x + px,center.y,center.z + pz);

			glm::vec2 uv = glm::vec2(x/(float)(widthCount-1),z/(float)(lenghtCount-1));

			vertices.push_back(point);
			uvs.push_back(uv);
		}
	}

	for(int y = 0; y < lenghtCount-1; y++)
	{
		for(int x = 0; x < widthCount-1; x++)
		{	
			int a = (x) + (y*widthCount);						//	a	b
			int b = (x+1) + (y*widthCount);						//	c 	d
			int c = (x) + ((y+1)*widthCount);
			int d = (x+1) + ((y+1)*widthCount);

			faces_indices.push_back(a);
			faces_indices.push_back(c);
			faces_indices.push_back(d);

			faces_indices.push_back(a);
			faces_indices.push_back(d);
			faces_indices.push_back(b);
		}
	}

	applyHeightMap();

	setupModel();
}

void Terrain::loadLevelTexture(char* path,int lvl)
{
	Texture* tex = new Texture(path);

	GLint useTextureLoc = glGetUniformLocation(programID, "useLevelTextures");
	glUniform1i(useTextureLoc, true);

	if(lvl == 0)
	{
		level0 = tex; 
		level0Uniform = glGetUniformLocation(programID,"texture0");
		glGenTextures(1,&level0Id);
		glBindTexture(GL_TEXTURE_2D,level0Id);
	}
	else if(lvl == 1)
	{
		level1 = tex;
		level1Uniform = glGetUniformLocation(programID,"texture1");
		glGenTextures(1,&level1Id);
		glBindTexture(GL_TEXTURE_2D,level1Id);
	}
	else
	{
		level2 = tex;
		level2Uniform = glGetUniformLocation(programID,"texture2");
		glGenTextures(1,&level2Id);
		glBindTexture(GL_TEXTURE_2D,level2Id);
	}

	glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,tex->getWidth(),tex->getHeight(),0,GL_RGB, GL_UNSIGNED_BYTE,tex->getData());

    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void Terrain::applyHeightMap()
{
	if(heightMap == nullptr){ return ;}

	peakHeight = 0;
	for(int i = 0; i < vertices.size();i++)
	{
		glm::vec2 uv = uvs.at(i);

		vertices.at(i).y += heightMap->get(uv.x,uv.y).x * maxHeight;

		peakHeight = max(peakHeight,vertices.at(i).y);
	}
}

Terrain::Terrain(glm::vec3 center, float width, float lenght, int wCount, int lCount,float maxHeight)
{
	this->width = width;
	this->lenght = lenght;
	this->center = center;
	this->maxHeight = maxHeight;

	heightMap = nullptr;
	modifyGeometry(wCount,lCount);
}
