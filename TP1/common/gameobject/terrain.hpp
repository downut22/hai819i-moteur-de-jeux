#ifndef TERRAIN_H
#define TERRAIN_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "gameobject.hpp"

#include "../texture.hpp"

using namespace std;

class Terrain : public GameObject {
private:
	int widthCount, lenghtCount;
	float width,lenght;
	glm::vec3 center;

	Texture* heightMap; float maxHeight; float peakHeight;

	Texture* level0; GLuint level0Id; GLuint level0Uniform;
	Texture* level1; GLuint level1Id; GLuint level1Uniform;
	Texture* level2; GLuint level2Id;GLuint level2Uniform;

public:

	void draw() override;
	void drawTextures() override;

	glm::vec2 getUV(glm::vec2 _position)
	{
		return glm::vec2( (_position.x + (width*0.5)) / width, (_position.y + (lenght*0.5))  / lenght);
	}

	float getHeight(glm::vec2 _uv)
	{
		if(_uv.x < 0 || _uv.x >= 1 || _uv.y < 0 || _uv.y >= 1){return 0;}
		return heightMap->get(_uv.x,_uv.y).x * maxHeight;
	}

	int getWidthCount(){return widthCount;}
	int getLenghtCount(){return lenghtCount;}
	float getMaxHeight(){return maxHeight;}
	float getPeakHeight(){return peakHeight;}
	glm::vec3 getCenter(){return center;}
	
	void modifyGeometry(int wCount, int lCount);

	void updateGeometry();

	void loadHeightMap(char* path)	{	heightMap = new Texture(path);	}

	void loadLevelTexture(char* path, int lvl);

	void applyHeightMap();

	Terrain(glm::vec3 center,float width, float lenght, int wCount, int lCount, float maxHeight);
};

#endif
