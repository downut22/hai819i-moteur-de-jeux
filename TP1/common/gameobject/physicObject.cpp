#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <common/objloader.hpp>
#include "physicObject.hpp"

using namespace std;

void PhysicObject::draw()
{
	GameObject::draw();
}

void PhysicObject::terrainCollision()
{
	double sy = m_position.y - (m_scale.y * 0.5);
	double ty = terrain->getCenter().y;

	if(sy <= ty)
	{
		velocity = bounce_coefficient * glm::reflect(velocity,glm::vec3(0,1,0));
	}
}

PhysicObject::PhysicObject(char* modelPath,Terrain* _terrain) : GameObject(modelPath)
{
	terrain = _terrain;
	gravity = glm::vec3(0,-9.81,0);
	bounce_coefficient = 1;
}
