#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <common/objloader.hpp>
#include "gameobject.hpp"

using namespace std;

void GameObject::drawTextures()
{
    glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,textureId);
	glUniform1i(textureUniform, 0);
}

void GameObject::draw()
{
    drawTextures();

    glUniformMatrix4fv(ModelID,1,GL_FALSE,&matrix()[0][0]);//&modelMatrix[0][0]);

	glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
                   	0,                  // attribute
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    0,                  // stride
                    (void*)0            // array buffer offset
                    );

    glBindBuffer(GL_ARRAY_BUFFER,uvbuffer);
    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,0,(void*)0);

    // Index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

    // Draw the triangles !
    glDrawElements(
                   	GL_TRIANGLES,      // mode
                    faces_indices.size(),    // count
                    GL_UNSIGNED_SHORT,   // type
                    (void*)0           // element array buffer offset
                    );

    glDisableVertexAttribArray(0);
}

void GameObject::bindTexture(Texture* tex, GLuint& id)
{
    if(tex != nullptr)
    {
        glGenTextures(1,&id);
        glBindTexture(GL_TEXTURE_2D,id);
        textureUniform = glGetUniformLocation(programID,"mainTexture");

        glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,tex->getWidth(),tex->getHeight(),0,GL_RGB, GL_UNSIGNED_BYTE,tex->getData());

        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

}

void GameObject::setupModel()
{
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

    
    glGenBuffers(1, &elementbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, faces_indices.size() * sizeof(unsigned short), &faces_indices[0] , GL_STATIC_DRAW);

    glGenBuffers(1,&uvbuffer);
    glBindBuffer(GL_ARRAY_BUFFER,uvbuffer);
    glBufferData(GL_ARRAY_BUFFER,uvs.size() * sizeof(glm::vec2),&uvs[0],GL_STATIC_DRAW);
}

GameObject::GameObject(char* modelPath) : Transform()
{
    loadOFF(modelPath, vertices, faces_indices, faces );
}

GameObject::GameObject(vector<glm::vec3> vertices,vector<std::vector<unsigned short>> faces,vector<unsigned short> faces_indices)  : Transform()
{
	this->vertices = vertices;
	this->faces = faces;
	this->faces_indices = faces_indices;
    this->texture = nullptr;
}

GameObject::~GameObject()
{
	glDeleteBuffers(1, &vertexbuffer);
    glDeleteBuffers(1, &elementbuffer);
}

/*
Plane::Plane(int xVertices, int zVertices)
{

}*/
