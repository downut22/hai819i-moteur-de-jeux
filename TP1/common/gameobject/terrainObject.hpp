#ifndef TERRAINOBJECT_H
#define TERRAINOBJECT_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "gameobject.hpp"
#include "terrain.hpp"

using namespace std;

class TerrainObject : public GameObject {
private:
	Terrain* terrain;

public:

	virtual void update(float deltatime) override
	{
		placeOnTerrain();
		GameObject::update(deltatime);
	}

	void draw() override;

	void placeOnTerrain();

	TerrainObject(char* modelPath,Terrain* _terrain);
};

#endif
