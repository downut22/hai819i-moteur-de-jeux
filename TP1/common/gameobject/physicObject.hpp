#ifndef PHYSICOBJECT_H
#define PHYSICOBJECT_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "gameobject.hpp"
#include "terrain.hpp"

using namespace std;

class PhysicObject : public GameObject {
private:
	Terrain* terrain;

	glm::vec3 velocity;

	glm::vec3 gravity;

	float bounce_coefficient;

public:

	virtual void update(float deltatime) override
	{
		updatePosition(deltatime);
		updateGravity(deltatime);
		terrainCollision();
		GameObject::update(deltatime);
	}

	void draw() override;

	void updatePosition(float deltatime){m_position += velocity * deltatime;}

	void updateGravity(float deltatime){velocity += gravity * deltatime;}

	void terrainCollision();

	void setVelocity(glm::vec3 vel) { velocity = vel;}

	void setBounce(float f) { bounce_coefficient = f;}

	PhysicObject(char* modelPath,Terrain* _terrain);
};

#endif
