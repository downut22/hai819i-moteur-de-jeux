#ifndef SCENE_H
#define SCENE_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../gameobject/transform.hpp"

using namespace std;

class Scene
{
protected:
	
	Transform* sceneRoot;
	vector<Transform*> childs;

public:

	Transform* getRoot() { return sceneRoot; }

	void bind(Transform* t) 
	{ 
		t->setParent(sceneRoot); 
		childs.push_back(t);
	}

	void update(float deltatime)
	{
		for(int i = 0; i < childs.size(); i++)
		{
			childs.at(i)->update(deltatime);
		}
	}

	Scene()
	{
		sceneRoot = new Transform(glm::vec3(0,0,0),glm::vec3(0,0,0),glm::vec3(1,1,1));
	}
	
};


/*
class Plane : public GameObject {
private:

public:

	Plane(int xVertices, int zVertices);
};

*/
#endif
