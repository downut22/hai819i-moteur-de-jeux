#ifndef PLANE_H
#define PLANE_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "gameobject.hpp"

using namespace std;

class Plane : public GameObject {
private:

public:

	Plane(int xVertices, int zVertices);
};

#endif
