#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace std;

class GameObject
{
private:
	vector<glm::vec3> vertices;
	vector<unsigned short> faces_indices;
	vector<std::vector<unsigned short> > faces;

	GLuint vertexbuffer;
	GLuint elementbuffer;

public:

	virtual void draw();

	void setupModel();

	GameObject(char* modelPath);

	GameObject(vector<glm::vec3> vertices,vector<std::vector<unsigned short>> faces,vector<unsigned short> faces_indices);

	virtual ~GameObject();
};

#endif
